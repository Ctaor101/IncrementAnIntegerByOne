﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication2
{
    class Answer
    {
        static void Main(string[] args)
        {
            //Add 1 to a number 

            List<int> numbers = new List<int>();
            numbers.Add(9);
            numbers.Add(9);
            numbers.Add(9);
            numbers.Add(9);
            numbers.Add(9);

            Console.Write("The original integer is: ");
            numbers.ForEach(x => Console.Write(x));
            
            var result = AddOneToNumberArrayList(numbers);
            PrintResult(result);
            
            Console.Read();
        }
              

        public static List<int> AddOneToNumberArrayList(List<int> test)
        {         
            if(test.Count == 0)
            {
                test.Add(1);
                return test;
            }

            //Check if last element is 9
            if(test[test.Count - 1] == 9)
            {
                test[test.Count - 1] = 0;

                //Check is next subsequent Element is 9 in loop
                for (int i = test.Count - 2; i >= 0; i--)
                {
                    //Subsequent Value is 9 but not the first index
                    if (test[i] == 9 && i != 0)
                    {
                        test[i] = 0;
                        continue;
                    }

                    //If Subsequent value is less then 9
                    if (test[i] != 9)
                    {
                        test[i] = test[i] + 1;
                        return test;
                    }

                    //If you made it to this point the other conditions havent been met. 
                    //9 must be in the first position.
                  
                    //Set first index to 0. 
                    test[0] = 0;

                    //The array must grow, new array to copy too.
                    var newResult = new List<int>();

                    //Add to the start of the array. 
                    newResult.Add(1);
                    newResult.AddRange(test);

                    return newResult;
                }                
            }

            //Last number was not 9 then add to end and complete.
            test[test.Count - 1] = test[test.Count - 1] + 1;
            return test;
           
        }

        public static void PrintResult(List<int> resultList)
        {
            Console.Write("The resulting number is: ");

            foreach(var res in resultList)
            {
                Console.Write(res);
            }            
        }

    }
}
